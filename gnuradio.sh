#!/bin/bash

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y git cmake g++ libboost-all-dev libgmp-dev swig python3-numpy python3-mako python3-sphinx python3-lxml doxygen libfftw3-dev libcomedi-dev libsdl1.2-dev libgsl-dev libqwt-qt5-dev libqt5opengl5-dev python3-pyqt5 liblog4cpp5-dev libzmq3-dev python3-yaml python3-click python3-click-plugins python3-gi python3-gi-cairo gir1.2-gtk-3.0

sudo mkdir /opt/gnuradio
sudo chown `id -u` /opt/gnuradio

cd /opt/gnuradio
git clone --recursive http://gnuradio.org/git/gnuradio.git src
cd src

patch gr-uhd/apps/uhd_fft <<END
--- uhd_fft.orig	2019-06-24 18:23:26.793828539 -0600
+++ uhd_fft	2019-06-24 18:23:58.666742913 -0600
@@ -82,7 +82,8 @@
         self.top_grid_layout = Qt.QGridLayout()
         self.top_layout.addLayout(self.top_grid_layout)
         self.settings = Qt.QSettings("GNU Radio", "uhd_fft")
-        self.restoreGeometry(self.settings.value("geometry"))
+        if self.settings.value("geometry") is not None:
+            self.restoreGeometry(self.settings.value("geometry"))

         ##################################################
         # Parameters
END
patch gr-uhd/apps/uhd_siggen_gui <<END
--- uhd_siggen_gui.orig	2019-06-24 18:23:35.134069293 -0600
+++ uhd_siggen_gui	2019-06-24 18:24:12.875228041 -0600
@@ -85,7 +85,8 @@
         self.top_grid_layout = Qt.QGridLayout()
         self.top_layout.addLayout(self.top_grid_layout)
         self.settings = Qt.QSettings("GNU Radio", "uhd_siggen_gui")
-        self.restoreGeometry(self.settings.value("geometry"))
+        if self.settings.value("geometry"):
+            self.restoreGeometry(self.settings.value("geometry"))

         ##################################################
         # Widgets + Controls
END

mkdir build
cd build
cmake -DENABLE_GRC=ON -DCMAKE_INSTALL_PREFIX:PATH=/opt/gnuradio ../
make

sudo make install

sudo sh -c "echo '/opt/gnuradio/lib' > /etc/ld.so.conf.d/gnuradio.conf"
sudo ldconfig -v

sudo sh -c "echo 'export PYTHONPATH=:/opt/gnuradio/lib/python3.6/dist-packages' > /etc/profile.d/gnuradio.sh"
sudo sh -c "echo 'export PATH=\$PATH:/opt/gnuradio/bin' >> /etc/profile.d/gnuradio.sh"
