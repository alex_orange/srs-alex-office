#!/bin/bash

if [[ -f /local/starting ]]
then
    exit 0
fi

set -e

touch /local/starting

sudo add-apt-repository -y ppa:ettusresearch/uhd
sudo apt-get update
sudo apt-get install -y libuhd-dev libuhd003 uhd-host

sudo "/usr/lib/uhd/utils/uhd_images_downloader.py" &

sudo apt-get install -y cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev
sudo apt-get install -y libboost-system-dev libboost-test-dev libboost-thread-dev libqwt-dev libqt4-dev

sudo sh -c "echo 'export PATH=\$PATH:/opt/srsLTE/bin' > /etc/profile.d/srslte.sh"

sudo sh -c "echo '/opt/srsLTE/lib' > /etc/ld.so.conf.d/srslte.conf"
sudo sh -c "echo '/opt/srsGUI/lib' >> /etc/ld.so.conf.d/srslte.conf"
sudo ldconfig -v

sudo patch /etc/sudoers <<END
--- sudoers.orig	2019-06-12 11:53:37.935114085 -0600
+++ sudoers	2019-06-12 11:53:46.815054925 -0600
@@ -8,7 +8,7 @@
 #
 Defaults	env_reset
 Defaults	mail_badpass
-Defaults	secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"
+Defaults	secure_path="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/srsLTE/bin"

 # Host alias specification
END

sudo mkdir /opt/srsLTE
sudo mkdir /opt/srsGUI
sudo chown `id -u` /opt/srsLTE
sudo chown `id -u` /opt/srsGUI

cd /opt/srsGUI
git clone https://github.com/srslte/srsgui src
cd src
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/srsGUI ../
make

sudo make install

cd /opt/srsLTE
git clone https://github.com/srsLTE/srsLTE src
cd src

patch cmake/modules/SRSLTE_install_configs.sh.in <<END
--- SRSLTE_install_configs.sh.in.orig	2019-06-13 12:01:43.519546410 -0600
+++ SRSLTE_install_configs.sh.in	2019-06-13 12:03:12.111198199 -0600
@@ -45,7 +45,8 @@
     # Set file ownership to user calling sudo
     if [ \$SUDO_USER ]; then
       user=\$SUDO_USER
-      chown \$user:\$user \$dest_path
+      group=\$SUDO_GID
+      chown \$user:\$group \$dest_path
     fi
   else
     echo " - \$source_path doesn't exists. Skipping it."
END

patch lib/src/phy/ue/ue_ul.c <<END
--- ue_ul.c.orig	2019-06-25 14:20:07.153153137 -0600
+++ ue_ul.c	2019-06-25 14:20:24.977239827 -0600
@@ -235,6 +235,7 @@
   if (amp*norm_factor < 0.1) {
     norm_factor = 0.1/amp;
   }
+  norm_factor *= 3.0;
   return norm_factor;
 }
 
END

mkdir build
cd build
cmake -DENABLE_GUI=ON -DPC_SRSGUI_INCLUDEDIR=/opt/srsGUI/include -DPC_SRSGUI_LIBDIR=/opt/srsGUI/lib -DCMAKE_INSTALL_PREFIX:PATH=/opt/srsLTE ../
make

sudo make install
sudo ./srslte_install_configs.sh service

sudo patch /etc/srslte/enb.conf <<END
--- /opt/srsLTE/share/srslte/enb.conf.example	2019-06-12 12:09:52.000000000 -0600
+++ enb.conf	2019-06-12 13:27:43.401517162 -0600
@@ -66,9 +66,9 @@
 #                     Default "auto". B210 USRP: 400 us, bladeRF: 0 us. 
 #####################################################################
 [rf]
-dl_earfcn = 3400
-tx_gain = 80
-rx_gain = 40
+dl_earfcn = 4650
+tx_gain = 31.5
+rx_gain = 31.5
 
 #device_name = auto
 #device_args = auto
END

sudo patch /etc/srslte/ue.conf <<END
--- /opt/srsLTE/share/srslte/ue.conf.example	2019-06-13 11:48:42.000000000 -0600
+++ ue.conf	2019-06-13 12:07:25.030456720 -0600
@@ -29,7 +29,7 @@
 #                     Default is auto (yes for UHD, no for rest)
 #####################################################################
 [rf]
-dl_earfcn = 3400
+dl_earfcn = 4650
 freq_offset = 0
 tx_gain = 80
 #rx_gain = 40
END

sudo ldconfig -v

touch /local/installed
