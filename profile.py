#!/usr/bin/python

"""
Two SDR nodes running srsLTE software with SDR hardware. You may request
that it runs over the air, or within the controlled (PhantomNet) environment.

Specifically srsGUI and srsLTE code is pre-installed:

https://github.com/srsLTE/srsGUI

https://github.com/srsLTE/srsLTE

Instructions:

**To run the EPC**

Open a terminal on the epc-enb node in your experiment. (Go to the "List View"
in your experiment. If you have ssh keys and an ssh client working in your
setup you should be able to click on the black "ssh -p ..." command to get a
terminal. If ssh is not working in your setup, you can open a browser shell
by clicking on the Actions icon corresponding to the node and selecting Shell
from the dropdown menu.)

Start up the EPC:

    sudo srsepc
    
**To run the eNodeB**

Open another terminal on the epc-enb node in your experiment.

Start up the eNodeB:

    sudo srsenb

**To run the UE**

Open a terminal on the ue node in your experiment.

Start up the UE:

    sudo srsue

**Verify functionality**

Open another terminal on the ue node in your experiment.

Verify that the virtual network interface tun_srsue" has been created:

    ifconfig tun_srsue

Run ping to the SGi IP address via your RF link:
    
    ping 172.16.0.1

Killing/restarting the UE process will result in connectivity being interrupted/restored.

If you are using an ssh client with X11 set up, you can run the UE with the GUI
enabled to see a real time view of the signals received by the UE:

    sudo srsue --gui.enable 1


"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig


x310_node_disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
b210_node_disk_image = \
        "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"

setup_commands = {
    False: "/local/repository/startup.sh",
    True: "/local/repository/startup.sh && /local/repository/gnuradio.sh"
}


def x310_node_pair(idx, x310_component_id, node_type):
    radio_link = request.Link("radio-link-%d"%(idx))
    radio_link.bandwidth = 10*1000*1000

    node = request.RawPC("x310-node-%d"%(idx))
    node.hardware_type = node_type
    node.disk_image = x310_node_disk_image
    node.addService(rspec.Execute(shell="bash", command=setup_command))

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("x310-%d"%(idx))
    radio.component_id = x310_component_id
    radio_link.addNode(radio)


def b210_nuc_pair(idx, aggregate, component_id):
    b210_nuc_pair_node = request.RawPC("b210-nuc-pair-%d"%(idx))
    b210_nuc_pair_node.component_manager_id = aggregate
    b210_nuc_pair_node.component_id = component_id

    b210_nuc_pair_node.disk_image = b210_node_disk_image
    b210_nuc_pair_node.addService(
        rspec.Execute(shell="bash", command=setup_command))
    


portal.context.defineParameter("x310_pair_nodetype",
                               "Type of the node paired with the X310 Radios",
                               portal.ParameterType.STRING, "d430")
portal.context.defineParameter("x310_radios",
                               "X310 Radio, comma separated list",
                               portal.ParameterType.STRING, "alex-pb")
b210_agg_desc = """B210/NUC Aggregate|component id, e.g.:
'urn:publicid:IDN+law73.powderwireless.net+authority+cm|nuc2'"""
b210_agg_def = "urn:publicid:IDN+law73.powderwireless.net+authority+cm|nuc2"

portal.context.defineParameter("b210_aggregate_component_list",
                               b210_agg_desc, portal.ParameterType.AGGREGATE,
                               b210_agg_def)
portal.context.defineParameter("build_gnuradio",
                               "Should GNU Radio be built as well?",
                               portal.ParameterType.BOOLEAN, False)

params = portal.context.bindParameters()

request = portal.context.makeRequestRSpec()

setup_command = setup_commands[params.build_gnuradio]

for i, x310_radio in enumerate(params.x310_radios.split(",")):
    if len(x310_radio) == 0:
        continue
    x310_node_pair(i, x310_radio, params.x310_pair_nodetype)

for i, b210_aggregate_component in \
        enumerate(params.b210_aggregate_component_list.split(",")):
    if len(b210_aggregate_component) == 0:
        continue
    b210_aggregate, b210_component_id = b210_aggregate_component.split("|")
    b210_nuc_pair(i, b210_aggregate, b210_component_id)


portal.context.printRequestRSpec()
